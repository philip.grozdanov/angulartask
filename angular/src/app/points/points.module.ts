import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PointscalculatorComponent } from './pointscalculator/pointscalculator.component';



@NgModule({
  declarations: [
    PointscalculatorComponent
  ],
  imports: [
    CommonModule
  ]
})
export class PointsModule { }
