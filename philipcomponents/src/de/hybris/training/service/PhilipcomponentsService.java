/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.training.service;

public interface PhilipcomponentsService
{
	String getHybrisLogoUrl(String logoCode);

	void createLogo(String logoCode);
}
