import { CmsComponent } from "@spartacus/core";

export interface CustomerPoints {
    points: number;
  }
  
  
  export interface CmsSlpCustomUserPointsComponent4 extends CmsComponent {
    title: string;
    headerText: string;
    footerText: string;
  }