import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PointscalculatorComponent } from './pointscalculator.component';

describe('PointscalculatorComponent', () => {
  let component: PointscalculatorComponent;
  let fixture: ComponentFixture<PointscalculatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PointscalculatorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PointscalculatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
