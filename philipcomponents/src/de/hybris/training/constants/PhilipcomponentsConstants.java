/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.training.constants;

/**
 * Global class for all Philipcomponents constants. You can add global constants for your extension into this class.
 */
public final class PhilipcomponentsConstants extends GeneratedPhilipcomponentsConstants
{
	public static final String EXTENSIONNAME = "philipcomponents";

	private PhilipcomponentsConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "philipcomponentsPlatformLogo";
}
