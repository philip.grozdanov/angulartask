/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.training.setup;

import static de.hybris.training.constants.PhilipcomponentsConstants.PLATFORM_LOGO_CODE;

import de.hybris.platform.core.initialization.SystemSetup;

import java.io.InputStream;

import de.hybris.training.constants.PhilipcomponentsConstants;
import de.hybris.training.service.PhilipcomponentsService;


@SystemSetup(extension = PhilipcomponentsConstants.EXTENSIONNAME)
public class PhilipcomponentsSystemSetup
{
	private final PhilipcomponentsService philipcomponentsService;

	public PhilipcomponentsSystemSetup(final PhilipcomponentsService philipcomponentsService)
	{
		this.philipcomponentsService = philipcomponentsService;
	}

	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
	public void createEssentialData()
	{
		philipcomponentsService.createLogo(PLATFORM_LOGO_CODE);
	}

	private InputStream getImageStream()
	{
		return PhilipcomponentsSystemSetup.class.getResourceAsStream("/philipcomponents/sap-hybris-platform.png");
	}
}
