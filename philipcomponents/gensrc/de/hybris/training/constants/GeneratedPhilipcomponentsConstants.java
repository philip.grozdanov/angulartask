/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Jun 14, 2022, 3:01:54 PM                    ---
 * ----------------------------------------------------------------
 */
package de.hybris.training.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated(since = "ages", forRemoval = false)
@SuppressWarnings({"unused","cast"})
public class GeneratedPhilipcomponentsConstants
{
	public static final String EXTENSIONNAME = "philipcomponents";
	
	protected GeneratedPhilipcomponentsConstants()
	{
		// private constructor
	}
	
	
}
