package de.hybris.training.controllers;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import de.hybris.platform.webservicescommons.swagger.ApiBaseSiteIdParam;
import org.springframework.web.bind.annotation.PathVariable;
import de.hybris.platform.webservicescommons.swagger.ApiFieldsParam;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.webservicescommons.mapping.FieldSetLevelHelper;

import java.util.List;

/**
 * Retrieve Customer points
 *
 * Class can be improved by creating separate service to retrieve the points
 * and have custom converter to convert the Customer object to CustomerPointsDTO object 
 */
@Controller
@ResponseBody
@ApiOperation(value = "Get Customer points")
@RequestMapping(value = "/{baseSiteId}/customerPoints")
@Api(tags = "Customer Points")
public class CustomerPointsController {

    private static final int ZERO_POINTS = 0;

    @Resource(name = "flexibleSearchService")
    private FlexibleSearchService flexibleSearchService;

    private static final String CUSTOMER_ID = "customerID";
    private static final String CUSTOMER_POINTS_QUERY = "SELECT {" + CustomerModel.PK
	+ "} FROM {" + CustomerModel._TYPECODE + "} WHERE {" + CustomerModel.CUSTOMERID + "}= ?" + CUSTOMER_ID;
	
    @RequestMapping(value = "/{customerID}", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(nickname = "getCustomerPoints", value = "Get customer points", notes = "Retrieve Customer points by given Customer ID")
    @ApiBaseSiteIdParam
    public CustomerPointsDTO getCustomerPoints(
            @ApiParam(value = "Customer ID", required = true)
            @PathVariable
            final String customerID,
            @ApiFieldsParam
            @RequestParam(defaultValue = FieldSetLevelHelper.DEFAULT_LEVEL)
            final String fields) {

        return getCustomer(customerID);

    }

	/**
	* Retrieve customer points for given customer ID
	@param customerID - customer ID 
	@return Object containing customer points which can be consumed by other APIs
	*/
    private CustomerPointsDTO getCustomer(String customerID) {

        final Map<String, Object> params = createQueryParameters(customerID);
        CustomerPointsDTO customerPointsDTO = new CustomerPointsDTO();
        List<Object> result = flexibleSearchService.search(CUSTOMER_POINTS_QUERY, params).getResult();
        if (result != null && result.size() > 0) {
            Object customerPoints = result.get(0);
            if (customerPoints instanceof CustomerModel) {
                CustomerModel customerModel = (CustomerModel) customerPoints;
                if (customerModel != null) {
                    Integer points = customerModel.getPoints();
                    customerPointsDTO.setPoints(points);
                } else {
                    customerPointsDTO.setPoints(ZERO_POINTS);
                }
            }
        } else {
            customerPointsDTO.setPoints(ZERO_POINTS);
        }

        return customerPointsDTO;
    }

	/**
	* Create HTTP request parameters 
	@param customerID - customer ID
	@return map containing all key value http parameters
	*/
    private Map<String, Object> createQueryParameters(String customerID) {
        final Map<String, Object> params = new HashMap<>();
        params.put(CUSTOMER_ID, customerID);
        return params;
    }

}