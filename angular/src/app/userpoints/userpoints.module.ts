import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserpointsComponent } from './userpoints.component';
import { RouterModule } from '@angular/router';
import { CmsPageGuard } from '@spartacus/storefront';
import { CmsConfig, ConfigModule } from '@spartacus/core';



@NgModule({
  declarations: [
    UserpointsComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: 'points',
        component: UserpointsComponent,
        canActivate: [CmsPageGuard],
        data: { pageLabel: 'homepage' }
      }
    ]),
    ConfigModule.withConfig({
      cmsComponents:
      {
        SlpCustomUserPointsComponent4: {
          component: UserpointsComponent,
        },

      },
    } as CmsConfig),

  ],
})
export class UserpointsModule { }
