/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.training.constants;

/**
 * Global class for all restphiliptest constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings({ "deprecation", "squid:CallToDeprecatedMethod" })
public final class RestphiliptestConstants extends GeneratedRestphiliptestConstants
{
	public static final String EXTENSIONNAME = "restphiliptest"; //NOSONAR

	private RestphiliptestConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
