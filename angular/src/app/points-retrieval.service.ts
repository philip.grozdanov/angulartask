import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserAccountFacade } from '@spartacus/user/account/root';
import { BASE_SITE_ID, CUSTOMER_ID, ELECTRONICS_SITE, HTTP_JSON_HEADERS, REST_URL } from './userpoints/rest-connection-constants';
import { CustomerPoints } from './userpoints/customer-points-interfaces';
@Injectable({
  providedIn: 'root'
})

export class PointsRetrievalService{



  constructor(private http: HttpClient,
    protected userAccount: UserAccountFacade) {

  }

  getPointsForCustomerId(userID: string): Observable<CustomerPoints>
   {
    {
      const url = `${REST_URL}${userID}`;
      let params = new HttpParams().set(CUSTOMER_ID, userID).set(BASE_SITE_ID, ELECTRONICS_SITE);
      const options = { params: params, headers: HTTP_JSON_HEADERS }
      return this.http.get<CustomerPoints>(url, options);
    }

  }




}
