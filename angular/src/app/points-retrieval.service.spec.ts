import { TestBed } from '@angular/core/testing';

import { PointsRetrievalService } from './points-retrieval.service';

describe('PointsRetrievalService', () => {
  let service: PointsRetrievalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PointsRetrievalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
