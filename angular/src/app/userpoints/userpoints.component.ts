import { Component, OnInit } from '@angular/core';
import { AuthService, CmsComponent, User, UserIdService, } from '@spartacus/core';
import { CmsComponentData } from '@spartacus/storefront';
import { UserAccountFacade } from '@spartacus/user/account/root';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { switchMap } from 'rxjs/operators';
import { PointsRetrievalService } from '../points-retrieval.service';
import { FOOTER_VALUE, HEADER_VALUE } from './component-labels';



export interface CustomerPoints {
  points: number;
}


export interface CmsSlpCustomUserPointsComponent4 extends CmsComponent {
  title: string;
  headerText: string;
  footerText: string;
}

@Component({
  selector: 'app-userpoints',
  templateUrl: './userpoints.component.html',
  styleUrls: ['./userpoints.component.scss']
})
export class UserpointsComponent implements OnInit {
  data$: Observable<CmsSlpCustomUserPointsComponent4> = this.component.data$;
  customerPoints$: Observable<number | undefined> | undefined;
  constructor(
    public component: CmsComponentData<CmsSlpCustomUserPointsComponent4>, private pointsRetrievalService: PointsRetrievalService,
    protected auth: AuthService,
    protected userAccount: UserAccountFacade,
    protected userIdService: UserIdService,
  ) { }




  ngOnInit(): void {
    console.log("init time")
    this.initCustomerPoints()
  }

  get headerText$(): Observable<string> {

    return this.component.data$.pipe((map(
      (data) => this.getTextFieldValue(data?.headerText, HEADER_VALUE))

    ));
  }


  get footerText$(): Observable<string> {
    return this.component.data$.pipe((map((data) => this.getTextFieldValue(data?.footerText, FOOTER_VALUE))));
  }

  getTextFieldValue(textFieldValue: string, defaultValue: string) {
    if (textFieldValue) {
      return textFieldValue
    }
    else {
      return defaultValue
    }
  }




  initCustomerPoints() {
    this.customerPoints$ = this.auth
      .isUserLoggedIn()
      .pipe(

        switchMap((isUserLoggedIn) =>
          isUserLoggedIn ? this.userAccount.get() : of(undefined)
        ),
        switchMap((user: User | undefined) => {
          if (!user || !user.customerId) return of(undefined);
          return this.pointsRetrievalService.getPointsForCustomerId(
            user.customerId
          );
        }),

        map((userPoints: CustomerPoints | undefined) =>
          userPoints?.points),
        catchError(err => {
          console.log('Unable to retrieve points', err);
          return throwError(err);
        })
      ),
      catchError(err => {
        console.log('Unable to retrieve points', err);
        return throwError(err);
      });

  }


}
