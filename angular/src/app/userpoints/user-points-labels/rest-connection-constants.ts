import { HttpHeaders } from "@angular/common/http";

export const ELECTRONICS_SITE = 'electronics';
export const CUSTOMER_ID = "customerID";
export const BASE_SITE_ID = "baseSiteId";
export const REST_URL = "https://localhost:9002/occ/v2/electronics/customerPoints/";


export const HTTP_JSON_HEADERS = new HttpHeaders({
  'Content-Type': 'application/json',
});
